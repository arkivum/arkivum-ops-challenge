# Simple Flask WebApp for Arkivum Ops interview challenge


## How to run the app

#### Requirements
* python 3.7 or greater
* pip installed for dependencies

#### How to run the webapp

1. Install the dependencies using pip: `pip install -r requirements.txt`
2. Run the app using flask: `SECRET_KEY="somethingsupersecret" FLASK_APP=./webapp/main.py python3 -m flask run`
3. Verify you can query the app
    ```
    curl http://localhost:5000
    Hello World.
    ```
4. You can upload an image (JPG recommended) and any exif data that exists will be returned in json format:
    ```
    curl http://localhost:5000/analyse -X POST -H 'Content-Type: multipart/form-data; charset=utf-8' -F "file=@./DJI_0002.JPG" {"DateTime":"2018:05:19 17:57:49","DateTimeDigitized":"2018:05:19 17:57:49","DateTimeOriginal":"2018:05:19 17:57:49","ImageDescription":"DCIM/100MEDIA/DJI_0002.JPG","Make":"DJI","Model":"FC1102","Software":"v00.00.1634"}
    ```


#### Monitoring
The application is currently exporting metrics via `http://localhost:5000/metrics`