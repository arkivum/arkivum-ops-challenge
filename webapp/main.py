from flask import Flask, request, redirect, flash, jsonify
from werkzeug.utils import secure_filename
from prometheus_flask_exporter import PrometheusMetrics
import random
import time
from PIL import Image
from PIL.ExifTags import TAGS
import os


SECRET_KEY = os.environ.get("SECRET_KEY")

if not SECRET_KEY:
    raise ValueError("No SECRET_KEY set for Flask application")

app = Flask(__name__)
app.secret_key = SECRET_KEY
metrics = PrometheusMetrics(app)

# static information as metric
metrics.info('app_info', 'Application info', version='0.0.1')

ALLOWED_EXTENSIONS = ['jpg', "tiff", "png", "jpeg"]

@app.route('/')
def main():
    return "Hello World.\n"

@app.route('/sleepy')
@metrics.gauge('in_progress', 'Long running requests in progress')
def long_running():
    sleep_duration = random.uniform(0, 4)
    time.sleep(sleep_duration)
    return "I slept for {:.2f} seconds and I feel GREAT!\n".format(sleep_duration)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/analyse', methods=["POST"])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            exif_data = image_exif(file)
            return jsonify(exif_data)
        else:
            return 'Invalid file uploaded', 400
    return "", 401


def image_exif(fileobj):
    image = Image.open(fileobj)
    exifdata = image.getexif()
    info = {}
    for tag_id in exifdata:
        tag = TAGS.get(tag_id, tag_id)
        data = exifdata.get(tag_id)
        if isinstance(data, str):
            info[tag] = data.strip("\u0000")
    image.close()
    return info

